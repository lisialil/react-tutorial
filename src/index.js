import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ToggleSwitch from './ToggleSwitch/ToggleSwitch';

const gridLocations = {
  0: "(1, 1)",
  1: "(2, 1)",
  2: "(3, 1)",
  3: "(1, 2)",
  4: "(2, 2)",
  5: "(3, 2)",
  6: "(1, 3)",
  7: "(2, 3)",
  8: "(3, 3)",
}

function Square(props) {
  return (
      <button
    className={props.class}
    onClick={props.onClick}
      >
      {props.value}
    </button>
  );
}

class Board extends React.Component {
  renderSquare(i) {
    const clss = this.props.winners.includes(i) ? "square winner" : "square";
    return <Square key={i}
    class={clss}
    value={this.props.squares[i]}
    onClick = {() => this.props.onClick(i)}
      />;
  }

  render() {
    let cell = 0;
    let createRow = () => {
      let row = [];
      for (let i = 0; i < 3; ++i) {
        row.push(this.renderSquare(cell++));
      }
      return row;
    };
    let rows = []
    for (let i = 0; i < 3; ++i) {
      rows.push(
          <div className="board-row" key={i}>{createRow()}</div>
      );
    }
    return <div>{rows}</div>;
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
      }],
      stepNumber: 0,
      xIsNext: true,
      locations: [""],
      selectedHistory: null,
      reversed: false,
    };
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const locations = this.state.locations.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1]; // this.state.stepNumber
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    const loc = " " + squares[i] + " moved to " + gridLocations[i];
    this.setState({
      // use concat not push to avoid mutating the original array
      history: history.concat([{squares: squares}]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
      locations: locations.concat(loc),
      selectedHistory: null,
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
      selectedHistory: step,
    });
  }

  onChange() {
    this.setState({
      reversed: !this.state.reversed,
    });
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner(current.squares);

    // Array.map((element, index) => { /* ... */ })
    let moves = history.map((step, move) => {
      const desc = move ?
            'Go to move #' + move :
            'Go to game start';
      if (this.state.selectedHistory && this.state.selectedHistory === move) {
        return (
          // list elements should have unique key to help React identify what has changed.
            <li key={move}>
            <button onClick={() => this.jumpTo(move)}><strong>{desc}</strong></button>
             <strong>{this.state.locations[move]}</strong>
          </li>
        );
      } else {
        return (
            <li key={move}>
            <button onClick={() => this.jumpTo(move)}>{desc}</button>
            {this.state.locations[move]}
          </li>
        );
      }
    });

    if (this.state.reversed) moves = moves.reverse();

    let status;
    let winners = [];
    if (winner) {
      status = 'Winner: ' + winner.winner;
      winners = winner.winners;
    } else if (history.length === 10) {
      status = 'The game is drawn.';
    } else {
      status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
    }
    return (
        <div className="game">
        <div className="game-board">
        <Board
      winners={winners}
      squares={current.squares}
      onClick={(i) => this.handleClick(i)}
        />
        </div>
        <div className="game-info">
        <div>{status}</div>
        <h2>Moves history</h2>
        <ToggleSwitch Name="moves" onChange={() => this.onChange()} />
        <ol>{moves}</ol>
        </div>
        </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);

function calculateWinner(squares) {
  const winningLines = [
    [0,1,2],
    [3,4,5],
    [6,7,8],
    [0,3,6],
    [1,4,7],
    [2,5,8],
    [0,4,8],
    [2,4,6]
  ];
  for (let i = 0; i < winningLines.length; ++i) {
    const [a,b,c] = winningLines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return {
        winner: squares[a],
        winners: winningLines[i],
      };
    }
  }
  return null;
}
